package com.example.task

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.layout_main.*

class Main : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_main)
        button_lookup.setOnClickListener {
            if (edt_city_name.getText().toString().isEmpty() || edt_city_name.getText().toString().length < 3) {
                Toast.makeText(this@Main, "Please enter City name length should be at least 3 characters", Toast.LENGTH_SHORT).show()
            } else {
                val `in` = Intent(this@Main, WeatherReportActivity::class.java)
                `in`.putExtra("CITYNAME", edt_city_name.getText().toString())
                startActivity(`in`)
            }
        }
    }
}