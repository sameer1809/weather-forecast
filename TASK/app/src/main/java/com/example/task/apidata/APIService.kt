package com.example.task.apidata

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface APIService {
    @POST("forecast")
    open fun getWeatherData(@Query("q") city: String?, @Query("appid") apiKey: String?): Call<ResponseBody?>?
}