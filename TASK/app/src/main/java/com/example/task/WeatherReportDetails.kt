package com.example.task

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.example.task.pojo.Weather
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_weather_report_details.*
import kotlinx.android.synthetic.main.toolbar.*

class WeatherReportDetails : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_weather_report_details)
        init()
    }

    private fun init() {
        if (intent.getStringExtra("DATA") != null) {
            val data = Gson().fromJson(intent.getStringExtra("DATA"), Weather::class.java)
            toolbar_header_text_title.text = data.cityName
            text_temp.text = data.main!!.temp.toString()
            text_feels_like.text = "Feels Like :  ${data.main!!.feels_like}"
            text_type.text = data.weather!!.get(0)!!.main
            text_type_des.text = data.weather!!.get(0)!!.description

        }
        toolbar_image_left.setOnClickListener(View.OnClickListener { onBackPressed() })
    }
}