package com.example.task.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task.R
import com.example.task.pojo.ListDataItemOBJ
import java.util.*

class RecyclerSimpleAdapter(var mContext: Context?, var mValues: ArrayList<ListDataItemOBJ?>?, var mlistener: ItemListener?) : RecyclerView.Adapter<RecyclerSimpleAdapter.ViewHolder?>() {

    class ViewHolder(v: View?) : RecyclerView.ViewHolder(v!!) {
        var title: TextView?
        var value: TextView?
        var container: LinearLayout?

        init {
            title = v!!.findViewById<TextView?>(R.id.text_title)
            value = v!!.findViewById<TextView?>(R.id.text_value)
            container = v!!.findViewById<LinearLayout?>(R.id.container)
        }
    }


    override fun getItemCount(): Int {
        return mValues!!.size
    }

    interface ItemListener {
        open fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_list_child, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder!!.title!!.text = mValues!!.get(position)!!.title
        holder!!.value!!.text = "Temp :  ${mValues!!.get(position)!!.value}"
        holder!!.container!!.setOnClickListener({ mlistener!!.onItemClick(position) })    }

}