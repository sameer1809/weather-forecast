package com.example.task

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.example.task.adapter.RecyclerSimpleAdapter
import com.example.task.apidata.APIService
import com.example.task.apidata.ApiUtils
import com.example.task.pojo.ListDataItemOBJ
import com.example.task.pojo.Weather
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_weather_data.*
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class WeatherReportActivity : Activity(), RecyclerSimpleAdapter.ItemListener {
    private var progressDialog: ProgressDialog? = null
    private var apiService: APIService? = null
    private var simpleAdapter: RecyclerSimpleAdapter? = null
    private val arrayListData: ArrayList<ListDataItemOBJ?>? = ArrayList()
    private var weatherData: Weather? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_weather_data)
        init()
    }

    private fun init() {
        apiService = ApiUtils.getAPIService()
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Please Wait...")
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)

        toolbar_header_text_title.text = intent.getStringExtra("CITYNAME")
        getWeatherData(intent.getStringExtra("CITYNAME"))
        toolbar_image_left.setOnClickListener(View.OnClickListener { onBackPressed() })
    }

    private fun setAdapter(data: Weather?) {
        weatherData = data
        weatherData!!.cityName = toolbar_header_text_title.text as String
        if (data!!.main != null) {
            val dataValue = ListDataItemOBJ()
            dataValue.title = "Clear"
            dataValue.value = data.main!!.temp.toString()
            val dataValue1 = ListDataItemOBJ()
            dataValue1.title = "Cloudy"
            dataValue1.value = data.main!!.temp_min.toString()
            val dataValue2 = ListDataItemOBJ()
            dataValue2.title = "Rain"
            dataValue2.value = data.main!!.humidity.toString()
            arrayListData!!.add(dataValue)
            arrayListData!!.add(dataValue1)
            arrayListData!!.add(dataValue2)
        }
        val manager = GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false)
        recycler_view.setLayoutManager(manager)
        simpleAdapter = RecyclerSimpleAdapter(this, arrayListData, this)
        recycler_view.setAdapter(simpleAdapter)
    }

    private fun getWeatherData(cityName: String?) {
        progressDialog!!.show()
        val result = apiService!!.getWeatherData(cityName, "65d00499677e59496ca2f318eb68c049")
        result!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>?) {
                progressDialog!!.dismiss()
                var Resp: String? = ""
                try {
                    if (response!!.code() == 200 && response.body() != null && response.body().toString().length > 0) {
                        Resp = response!!.body()!!.string()
                        Log.v("", "Resp$Resp")
                        val jsonArray = JSONArray(JSONObject(Resp).getString("list"))
                        if (jsonArray.length() > 0) {
                            setAdapter(Gson().fromJson(jsonArray.getJSONObject(0).toString(), Weather::class.java))
                        }
                    } else {
                        showAlert("Server Connection Failed" + response.code())
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    progressDialog!!.dismiss()
                    showAlert("Server Connection Failed")
                } catch (e: JSONException) {
                    e.printStackTrace()
                    progressDialog!!.dismiss()
                    showAlert("Server Connection Failed")
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                progressDialog!!.dismiss()
                showAlert("Server Connection Failed")
            }
        })
    }

    private fun showAlert(msg: String?) {
        val myDialog = AlertDialog.Builder(this)
        val title = TextView(this)
        title.text = "Alert"
        title.setPadding(10, 20, 10, 10)
        title.gravity = Gravity.CENTER
        title.setTextColor(resources.getColor(R.color.colorPrimary))
        title.textSize = 20f
        myDialog.setCustomTitle(title)
        myDialog.setMessage(msg)
        myDialog.setCancelable(false)
        myDialog.setPositiveButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(arg0: DialogInterface?, arg1: Int) {
                run {}
            }
        })
        myDialog.show()
    }

    override fun onItemClick(position: Int) {
        val `in` = Intent(this@WeatherReportActivity, WeatherReportDetails::class.java)
        `in`.putExtra("DATA", Gson().toJson(weatherData))
        startActivity(`in`)
    }
}