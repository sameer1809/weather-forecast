package com.example.task.apidata

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiUtils {
    val BASE_URL: String? = "https://api.openweathermap.org/data/2.5/"
    private var retrofit: Retrofit? = null
    fun getAPIService(): APIService? {
        Log.v("", "BASE_URL--$BASE_URL")
        return getClient()!!.create(APIService::class.java)
    }

    fun getClient(): Retrofit? {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        if (retrofit == null) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor(logging)
                    .build()
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit
    }
}