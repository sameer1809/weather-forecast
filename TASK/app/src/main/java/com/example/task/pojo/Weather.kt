package com.example.task.pojo

import java.util.*

class Weather {
    var cityName : String? = null
    var main: Main? = Main()
    var wind: Wind? = Wind()
    var rain: Sys? = Sys()
    var weather: ArrayList<WeatherData?>? = ArrayList()
    var clouds: Clouds? = Clouds()

    inner class WeatherData {
         val id = 0
         val main: String? = null
         val description: String? = null
         val icon: String? = null
    }

    inner class Main {
         var temp : Float? = null 
         var feels_like: Float? = null
         var temp_min: Float? = null
         var temp_max: Float? = null
         var pressure: Float? = null
         var grnd_level: Float? = null
         var humidity: Float? = null
         var temp_kf: Float? = null
      
    }

    inner class Wind {
         var speed = 0f
         var deg = 0f

    }

    inner class Sys {
         var pod: String? = null

    }

    inner class Clouds {
         var all = 0

    }
}